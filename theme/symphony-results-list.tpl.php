<?php
/**
 * @file
 * Template file for the symphony table results display.
 *
 * Variables available:
 * $results: The results array (the SOAP response transformed into an array)
 *
 * The table header is always needed, even if empty, for the pagination plugin
 *
 */
?>
<div class="view">
<?php if (count($results)==0): ?>
  <div class="views-row">
    <article class="node node-event node-teaser">
      <div class="content">No events found</div>
    </article>
  </div>
<?php else: ?>
  <?php foreach ($results as $result): ?>
    <div class="views-row">
      <article class="node node-event node-teaser">
        <header>
          <h1><?php print drupal_render($result['event_display_link']); ?></h1>
          <div class="details">
          <?php if(!empty($result['start_date_formatted'])): ?>
            <div class="date"><?php print $result['start_date_formatted']; ?></div>
          <?php endif; ?>
            <?php if (!empty($result['institute_link']) && !$result['institute_link']['#printed']): ?>
              <div class="organisation"><?php print drupal_render($result['institute_link']); ?></div>
            <?php endif; ?>
            <?php if (!empty($result['series_link']) && !$result['series_link']['#printed']): ?>
              <div class="organisation series"><?php print drupal_render($result['series_link']); ?></div>
            <?php endif; ?>
          </div>
        </header>
        <?php if(!empty($result['ResultDescription'])): ?>
          <div class="content"><?php print $result['ResultDescription']; ?></div>
        <?php endif; ?>
      </article>
    </div>
  <?php endforeach; ?>
<?php endif; ?>
</div>